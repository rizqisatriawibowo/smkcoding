import React, { Component } from 'react';

import IndexView from '../view/IndexView'

class Index extends Component {

    constructor() {

        this.state = {
            nama: "",
            umur: 0
        }

    }

    componentDidMount = () => {
        this.setName()
    }

    setName = () => {
        this.setState({
            nama: "Hello World"
        })
    }

    render() {
        return (
            <IndexView stateIndex={this.state} />
        );
    }
}

export default Index;